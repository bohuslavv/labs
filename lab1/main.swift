// // ЗАВДАННЯ No1 // //

//
//  main.swift
//  Hello_World
//
//  Created by Bohuslav Pavlyshynets on 18.11.2020.
//

import Foundation

print("Hello, World!")



// // ЗАВДАННЯ No2 // //

var integer1: UInt8
integer1 = 12
var integer2: Int8
integer2 = -100

var hexadecimal_integer = 0x80 // decimal 128

var max_decimal_number_that_16_bits_can_contain = -65535

var max_decimal_integer_number: UInt64
max_decimal_integer_number = 18446744073709551615

var lest_float_decimal: Float
lest_float_decimal = 10235.34

var this_is_a_char = "a"

var a_string = "Hello World"

var the_truth = true

var digital_twelve = 12
var string_twelve = "twelve"



// // ЗАВДАННЯ No3 // //

var someString = "Hello World. This is Swift programming language"
print("someString: \n\n", someString)
print("someString has \(someString.count) characters\n\n")

someString = someString.replacingOccurrences(of: "i", with: "u", options: .literal, range: nil)

print("SomeString after i to u modification: \n\n\(someString)")

someString.remove(at: someString.index(someString.startIndex, offsetBy: 10))
someString.remove(at: someString.index(someString.startIndex, offsetBy: 7))
someString.remove(at: someString.index(someString.startIndex, offsetBy: 4))


print("SomeString after deletion of characters at 4, 7, 10 indexes: \n\n\(someString)")

someString +=  " (modified)"

print("SomeString after adding of (modified): \n\n\(someString)")

print("Is SomeString empty?: \(someString.isEmpty)")

someString += "."

print("\nThe someString begins with 'Hello' prefix? : ", someString.hasPrefix("Hello"))
print("\nThe someString end with 'world.' prefix? : ", someString.hasSuffix("world."))


let index = someString.index(someString.startIndex, offsetBy: 10)  //here you define a place (index) to insert at
someString.insert("-", at: index)  //and here you insert

print("\nThe someString with inserted '-': ", someString)

someString = someString.replacingOccurrences(of: "Thus us", with: "It is")

print("\nThe someString with replaced 'Thus us' with 'It is': ", someString)


print("SomeString characters at the following positions 10: \(someString[someString.index(someString.startIndex, offsetBy: 10)]), 15: \(someString[someString.index(someString.startIndex, offsetBy: 15)])")


let index_of_the_beginning_of_the_substring = someString.index(someString.startIndex, offsetBy: 10)
let index_of_the_end_of_the_substring = someString.index(someString.startIndex, offsetBy: 15)



print("\nSubstring of SomeString between characters at positions 10 & 15: \(someString[index_of_the_beginning_of_the_substring ..< index_of_the_end_of_the_substring])")



// // ЗАВДАННЯ No4 // //

var integerNumber: Int?
var decimalNumber: Float?
integerNumber = 935684
integerNumber! += 935684
integerNumber! = -integerNumber!

decimalNumber = Float(integerNumber!)


//var pairOrValues: Array? = [decimalNumber as Any, integerNumber as Any]
var pairOfValues: [Int: Float]? = [integerNumber!: decimalNumber!]
if !pairOfValues!.isEmpty {
    for intVal in pairOfValues!.keys {
        print("pairOfValues integer value: \(intVal)")
    }
    for decVal in pairOfValues!.values {
        print("pairOfValues decimal value: \(decVal)")
    }
}
